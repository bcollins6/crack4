#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "md5.h"
#include "entry.h"

int main(int argc, char *argv[])
{
    // Check number of arguments
    if (argc < 3)
    {
        printf("Usage: %s plain_file rainbow_file\n", argv[0]);
        exit(1);
    }
    
    // Open dictionary for reading in text mode
    FILE *df = fopen(argv[1], "r");
    if (!df)
    {
        printf("Can't open %s for reading.\n", argv[1]);
        exit(1);
    }
    
    // Open rainbow file for writing and reading in binary mode
    FILE *rf = fopen(argv[2], "w+b");
    if (!rf)
    {
        printf("Can't open %s for writing.\n", argv[2]);
        exit(1);
    }

    char pw[21];
    int count = 0;
    int loc = 0;
    
    struct entry blank = { .pass = ""};
    
    while (fgets(pw, 21, df) != NULL)
    {
        // Trim newline
        pw[strcspn(pw, "\n")] = '\0';
        
        // Hash it
        unsigned char *ph = md5digest(pw, strlen(pw));
        
        // Put hash and plain into entry
        struct entry e;
        memcpy(e.hash, ph, 16);
        strcpy(e.pass, pw);
        
        // Extract first 3 bytes and make an int out of them.
        int idx = ph[0] * 65536 + ph[1] * 256 + ph[2];
               
        // Store entry at that location in file
        int file_location = idx * sizeof(struct entry);
        struct entry tmp;
        
        // Seek to the file location
        if (fseek(rf, file_location, SEEK_SET) < 0)
        {
            printf("fseek: %s\n", strerror(errno));
        }
        
        // Find the first unused slot
        int fread_result;
        do
        {
            // Read the entry here
            fread_result = fread(&tmp, sizeof(struct entry), 1, rf);
        }
        while (fread_result && memcmp(&blank, &tmp, sizeof(struct entry)) != 0);
            
        // Found a blank spot. Need to return to it.  
        if (fread_result)
        {
            if (fseek(rf, -sizeof(struct entry), SEEK_CUR) < 0)
                printf("Can't fseek back 1 entry\n");
        }
        
        // Write to disk
        if (!fwrite(&e, sizeof(struct entry), 1, rf))
            printf("Can't write to file\n");
        
        if (count % 10000 == 0) printf("%d\n", count);
        count++;
        loc += strlen(pw) + 1;
    }
    fclose(rf);
    fclose(df);
}
